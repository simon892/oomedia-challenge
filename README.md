# ooMedia Coding test

  

## Background

This project is built using the MERN stack. The initial boilerplate is provider by `(https://github.com/arkon/MERN-boilerplate)`

  

## Setup and Startup

  

#### Development

1. Download MongoDB from `(https://www.mongodb.com/download-center)`

2. When installing MongoDB use the default setting. This project connects to MongoDB using the default connection settings. See `config/.env`.

3.  Start MongoDB from the terminal if using macOS or run the mongod.exe file if on windows.

4.  `npm install`

5.  `npm run start:dev`

6. Go to `http://localhost:8080/`

  

#### Production

1. Signup to MongoDB cloud service

2. Add database settings in `config/.env`.

3.  `npm install`

4.  `npm run start`

  

## Unit Tests

Unit testing is performed using Mocha and Sinon.

To run test : `npm run test`

  

## Backend

The backend uses Express for handling routes and connecting to MongoDB.

Before data is saved to the database, `express-validator` is used to validate the data. See `server/validations` for validation logic.

Only certain characters are allowed to be saved. See the whitelisted characters at `server/validations/config/whitelist.js`.

Routes are designed around the REST principles.

Route logic is performed by the route handlers. Unit tests is performed on each handler. See testing.

Due to time constraints the following are a list of features or improvements that could be made:

#### ToDo

1. Authentication and Authorization: Consideration for this feature was given when setting up the the code. If necessary and I can find the time I can implement this. Let me know.

2. Remove console.logs and put in a better logging system, i.e. Winston.

3. Add unit tests to validation logic.

4. Add better stack tracing in development environment.

  

## Frontend

As the main focus of this test was the backend, not a lot of consideration was given to the structure and styling of the frontend code.

The frontend consists of three pages. The homepage which list the current inventory, the add shopping page and the add assets page.

Users should begin by going to the 'add shopping' page to add the first shopping centre.

Forms errors are validated by the server and presented to the users.

#### ToDo

1. Structure of code: Reusable UI components should be abstracted into a components folder.

2. Styling: Implement styling on each page.

3. Client-side validations: To better demonstrate the server error handling, client-side form validation was not implemented.

4. Add unit tests to UI components and snapshot testing to the pages.

5. Add UI to enable editing of Assets and Shopping centres.