const mongoose = require('mongoose');

const AddressSchema = new mongoose.Schema({
  street: {
    type: String,
    default: '',
  },
  city: {
    type: String,
    default: '',
  },
  state: {
    type: String,
    default: '',
  },
});
const ShoppingCentresSchema = new mongoose.Schema({
  name: {
    type: String,
    default: '',
  },
  address: AddressSchema,
});

module.exports = mongoose.model('ShoppingMalls', ShoppingCentresSchema);
