const mongoose = require('mongoose');

const DimensionSchema = new mongoose.Schema({
  width: {
    type: String,
    default: '',
  },
  length: {
    type: String,
    default: '',
  },
});

const LocationSchema = new mongoose.Schema({
  mapRef: {
    type: String,
    default: '',
  },
  floorLevel: {
    type: String,
    default: '',
  },
});

const AssetsSchema = new mongoose.Schema({
  name: {
    type: String,
    default: '',
  },
  shoppingcentreId: {
    type: String,
    default: '',
  },
  status: {
    type: Boolean,
    default: false,
  },
  dimensions: DimensionSchema,
  location: LocationSchema,
});

module.exports = mongoose.model('Assets', AssetsSchema);
