
module.exports = {
  nameRequired: 'Name of shopping mall is required',
  streetRequired: 'Street of shopping is required',
  cityRequired: 'City of shopping is required',
  stateRequired: 'State of shopping is required',
  valueType: 'Some fields are incorrect',
  incorrectState: 'State is not valid',
  assestNameRequired: 'Name of asset is required',
  shopIdRequired: 'A valid shopping centre needs to be selected from the dropdown',
  levelRequired: 'Level of asset is required',
  mapRefRequired: 'Map reference of asset is required',
  widthRequired: 'Width of asset is required',
  lengthRequired: 'Length of asset is required',
};
