
const { body } = require('express-validator/check');
const msg = require('./config/msg');
const whitelist = require('./config/whitelist');

const addAssets = [
  body('name')
    .not().isEmpty()
    .withMessage(msg.assestNameRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('shoppingcentreId')
    .not().isEmpty()
    .withMessage(msg.shopIdRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('dimensions.width')
    .not().isEmpty()
    .withMessage(msg.widthRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('dimensions.length')
    .not().isEmpty()
    .withMessage(msg.lengthRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('location.mapRef')
    .not().isEmpty()
    .withMessage(msg.mapRefRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('location.floorLevel')
    .not().isEmpty()
    .withMessage(msg.levelRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
];

const updateAssets = [
  body('name')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('shoppingcentreId')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('dimensions.width')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('dimensions.length')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('location.mapRef')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('location.floorLevel')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
];


module.exports = {
  addAssets,
  updateAssets,
};
