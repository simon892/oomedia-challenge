
const { body } = require('express-validator/check');
const msg = require('./config/msg');
const whitelist = require('./config/whitelist');

const addShoppingCentres = [
  body('name')
    .not().isEmpty()
    .withMessage(msg.nameRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('address.street')
    .not().isEmpty()
    .withMessage(msg.streetRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('address.city')
    .not().isEmpty()
    .withMessage(msg.cityRequired)
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('address.state')
    .whitelist(whitelist.characters)
    .custom((value) => {
      if (!['ACT', 'NSW', 'QLD', 'VIC', 'NT', 'SA', 'TAS', 'WA'].includes(value)) {
        throw new Error(msg.incorrectState);
      } else {
        return true;
      }
    })
    .isString()
    .withMessage(msg.valueType),
];

const updateShoppingCentres = [
  body('name')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('address.street')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('address.city')
    .whitelist(whitelist.characters)
    .isString()
    .withMessage(msg.valueType),
  body('address.state')
    .whitelist(whitelist.characters)
    .custom((value) => {
      if (!['ACT', 'NSW', 'QLD', 'VIC', 'NT', 'SA', 'TAS', 'WA'].includes(value)) {
        throw new Error(msg.incorrectState);
      } else {
        return true;
      }
    })
    .isString()
    .withMessage(msg.valueType),
];


module.exports = {
  addShoppingCentres,
  updateShoppingCentres,
};
