const validate = require('../../validations/shoppingCenters');
const { validationHandle, shoppingCentresHandle } = require('../../routeHandlers/index');

module.exports = (app) => {
  app.get('/api/shoppingcentres',
    shoppingCentresHandle.getShoppingCentres);

  app.post('/api/shoppingcentres',
    validate.addShoppingCentres,
    validationHandle,
    shoppingCentresHandle.addShoppingCentres);


  app.put('/api/shoppingcentres/:id',
    validate.updateShoppingCentres,
    validationHandle,
    shoppingCentresHandle.editShoppingCentres);
};
