const validate = require('../../validations/assets');
const { validationHandle, assetsHandle } = require('../../routeHandlers/index');

module.exports = (app) => {
  app.get('/api/assets', assetsHandle.getAssets);
  app.get('/api/shoppingcentres/:id/assets', assetsHandle.getAssetsByShopId);

  app.post('/api/assets', validate.addAssets,
    validationHandle,
    assetsHandle.addAssets);

  app.put('/api/assets/:id', validate.updateAssets,
    validationHandle,
    assetsHandle.editAssets);
};
