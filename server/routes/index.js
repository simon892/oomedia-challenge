const fs = require('fs');

module.exports = (app) => {
  // API routes
  fs.readdirSync(`${__dirname}/endpoints/`).forEach((file) => {
    require(`./endpoints/${file.substr(0, file.indexOf('.'))}`)(app);
  });
};
