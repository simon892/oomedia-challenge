const validationHandle = require('./handlers/validations');
const shoppingCentresHandle = require('./handlers/shoppingcentres');
const assetsHandle = require('./handlers/assets');


module.exports = {
  validationHandle,
  shoppingCentresHandle,
  assetsHandle,
};
