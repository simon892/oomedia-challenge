
const ShoppingCentres = require('../../models/ShoppingCentres');

module.exports = {
  getShoppingCentres: (req, res, next) => {
    ShoppingCentres.find()
      .exec()
      .then(Shoppingcentres => res.json(Shoppingcentres))
      .catch(err => next(err));
  },

  addShoppingCentres: (req, res, next) => {
    const { name, address } = req.body;
    const newCentres = new ShoppingCentres({ name, address });
    return newCentres.save()
      .then(() => res.json(newCentres))
      .catch(err => next(err));
  },

  editShoppingCentres: (req, res, next) => {
    // Only update data that is in req body
    const address = req.body.address
      ? {
        ...(req.body.address.street && { state: req.body.address.street }),
        ...(req.body.address.city && { state: req.body.address.city }),
        ...(req.body.address.state && { state: req.body.address.state }),
      } : null;
    const updateData = {
      ...(req.body.name && { name: req.body.name }),
      ...(req.address && { address }),
    };
    return ShoppingCentres.findOneAndUpdate({ _id: req.params.id }, {
      $set: {
        ...updateData,
      },
    }).exec()
      .then(Shoppingcentres => res.json(Shoppingcentres))
      .catch(err => next(err));
  },
};
