
const Assets = require('../../models/Assets');
const ShoppingCentres = require('../../models/ShoppingCentres');

module.exports = {
  getAssets: (req, res, next) => {
    Assets.find()
      .exec()
      .then(assets => res.json(assets))
      .catch(err => next(err));
  },

  getAssetsByShopId: (req, res, next) => {
    Assets.find({ shoppingcentreId: req.params.id })
      .exec()
      .then(assets => res.json(assets))
      .catch(err => next(err));
  },

  addAssets: (req, res, next) => {
    const {
      name, shoppingcentreId, dimensions, location, status,
    } = req.body;

    const newAsset = new Assets({
      name, dimensions, location, shoppingcentreId, status,
    });
    const newAssetId = newAsset.id;

    return newAsset.save()
      .then(() => res.json(newAsset))
      .then(() => {
        ShoppingCentres.findOneAndUpdate(
          { _id: shoppingcentreId },
          { $push: { assets: newAssetId } },
        ).exec();
      })
      .catch(err => next(err));
  },

  editAssets: (req, res, next) => {
    // Only update data that is in req body
    const dimensions = req.body.dimensions
      ? {
        ...(req.body.dimensions.width && { state: req.body.dimensions.width }),
        ...(req.body.dimensions.length && { state: req.body.dimensions.length }),
      } : null;

    const location = req.body.location
      ? {
        ...(req.body.location.mapRef && { state: req.body.location.mapRef }),
        ...(req.body.location.floorLevel && { state: req.body.location.floorLevel }),
      } : null;

    const updateData = {
      ...(req.body.name && { name: req.body.name }),
      ...(req.body.shoppingcentreId && { name: req.body.shoppingcentreId }),
      ...(req.body.status && { name: req.body.status }),
      ...(req.dimensions && { dimensions }),
      ...(req.location && { location }),
    };

    return Assets.findOneAndUpdate({ _id: req.params.id }, {
      $set: {
        ...updateData,
      },
    }).exec()
      .then(Shoppingcentres => res.json(Shoppingcentres))
      .catch(err => next(err));
  },
};
