
const sinon = require('sinon');
const AssetsHandle = require('../assets');
const Assets = require('../../../models/Assets');
const ShoppingCentres = require('../../../models/ShoppingCentres');

describe('Assets', () => {
  let req;
  let mockFindOneAndUpdate;
  const errorText = 'errorText';
  const res = {
    json: sinon.spy(),
  };
  const next = sinon.spy();
  const assetData = {
    name: 'Aldi Ads',
    shoppingcentreId: '1234567',
    status: true,
    dimensions: { width: '200', length: '300' },
    location: { mapRef: '2A', floorLevel: '3' },
  };
  const shoppingCentreData = {
    name: 'Aldi',
    address: { street: 'aldi street', city: 'Canberra', state: 'ACT' },
  };
  req = { params: { } };

  describe('getAssets route handler', () => {
    beforeEach(() => {
      const mockFindOne = {
        exec: sinon.stub().onFirstCall().resolves(assetData).onSecondCall()
          .rejects(errorText),
      };
      sinon.stub(Assets, 'find').returns(mockFindOne);
    });

    afterEach(() => {
      Assets.find.restore();
    });

    it('should return the expected data ', async () => {
      await AssetsHandle.getAssets(req, res);
      sinon.assert.calledWith(res.json, assetData);
    });

    it('should pass the error text if an error is thrown', async () => {
      try {
        await AssetsHandle.getAssets(req, res, next);
      } catch {
        sinon.assert.calledWith(next, errorText);
      }
    });
  });

  describe('getAssetsByShopId route handler', () => {
    beforeEach(() => {
      const mockFindOne = {
        exec: sinon.stub().onFirstCall().resolves(assetData).onSecondCall()
          .rejects(errorText),
      };
      sinon.stub(Assets, 'find').returns(mockFindOne);
      req = { params: { id: 'testId' } };
    });

    afterEach(() => {
      Assets.find.restore();
    });

    it('should return the expected data ', async () => {
      await AssetsHandle.getAssetsByShopId(req, res);
      sinon.assert.calledWith(res.json, assetData);
    });

    it('should pass the error text if an error is thrown', async () => {
      try {
        await AssetsHandle.getAssetsByShopId(req, res, next);
      } catch {
        sinon.assert.calledWith(next, errorText);
      }
    });
  });

  describe('addAssets route handler', () => {
    beforeEach(() => {
      mockFindOneAndUpdate = {
        exec: sinon.stub().resolves(shoppingCentreData),
      };
      sinon.stub(ShoppingCentres, 'findOneAndUpdate').returns(mockFindOneAndUpdate);
      sinon.stub(Assets.prototype, 'save').onFirstCall().resolves(assetData)
        .onSecondCall()
        .rejects(errorText);
      req = {
        body: {
          ...assetData,
        },
        params: {},
      };
    });

    afterEach(() => {
      ShoppingCentres.findOneAndUpdate.restore();
      Assets.prototype.save.restore();
    });

    it('should return the expected data ', async () => {
      await AssetsHandle.addAssets(req, res);
      // As mongoose injects ids into return response we only need to matchs some keys.
      sinon.assert.calledWith(res.json, sinon.match(assetData));
      mockFindOneAndUpdate.exec.returns(shoppingCentreData);
    });

    it('should pass the error text if an error is thrown', async () => {
      try {
        await AssetsHandle.addAssets(req, res, next);
      } catch {
        sinon.assert.calledWith(next, errorText);
      }
    });
  });

  describe('editAssets route handler', () => {
    beforeEach(() => {
      mockFindOneAndUpdate = {
        exec: sinon.stub().onFirstCall().resolves(assetData).onSecondCall()
          .rejects(errorText),
      };
      sinon.stub(Assets, 'findOneAndUpdate').returns(mockFindOneAndUpdate);
      req = {
        body: {
          ...assetData,
        },
        params: { id: 'testId' },
      };
    });

    afterEach(() => {
      Assets.findOneAndUpdate.restore();
    });

    it('should return the updated data ', async () => {
      await AssetsHandle.editAssets(req, res);
      sinon.assert.calledWith(res.json, assetData);
    });

    it('should pass the error text if an error is thrown', async () => {
      try {
        await AssetsHandle.editAssets(req, res, next);
      } catch {
        sinon.assert.calledWith(next, errorText);
      }
    });
  });
});
