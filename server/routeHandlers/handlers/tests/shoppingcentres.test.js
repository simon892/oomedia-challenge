
const sinon = require('sinon');
const ShoppingCentresHandle = require('../shoppingcentres');
const ShoppingCentres = require('../../../models/ShoppingCentres');

describe('Shoppingcentres', () => {
  let req;
  const errorText = 'errorText';
  const shoppingCentreData = {
    name: 'Aldi',
    address: { street: 'aldi street', city: 'Canberra', state: 'ACT' },
  };
  const res = {
    json: sinon.spy(),
  };
  const next = sinon.spy();
  req = { params: { } };
  describe('getShoppingCentres route handler', () => {
    beforeEach(() => {
      const mockFindOne = {
        exec: sinon.stub().onFirstCall().resolves(shoppingCentreData).onSecondCall()
          .rejects(errorText),
      };
      sinon.stub(ShoppingCentres, 'find').returns(mockFindOne);
    });

    afterEach(() => {
      ShoppingCentres.find.restore();
    });

    it('should return the expected data ', async () => {
      await ShoppingCentresHandle.getShoppingCentres(req, res);
      sinon.assert.calledWith(res.json, shoppingCentreData);
    });

    it('should pass the error text if an error is thrown', async () => {
      try {
        await ShoppingCentresHandle.getShoppingCentres(req, res, next);
      } catch {
        sinon.assert.calledWith(next, errorText);
      }
    });
  });

  describe('addShoppingCentres route handler', () => {
    beforeEach(() => {
      sinon.stub(ShoppingCentres.prototype, 'save').onFirstCall().resolves(shoppingCentreData)
        .onSecondCall()
        .rejects(errorText);
      req = {
        body: {
          ...shoppingCentreData,
        },
        params: {},
      };
    });

    afterEach(() => {
      ShoppingCentres.prototype.save.restore();
    });

    it('should return the expected data ', async () => {
      await ShoppingCentresHandle.addShoppingCentres(req, res);
      // As mongoose injects ids into return response we only need to matchs some keys.
      sinon.assert.calledWith(res.json, sinon.match(shoppingCentreData));
    });

    it('should pass the error text if an error is thrown', async () => {
      try {
        await ShoppingCentresHandle.addShoppingCentres(req, res, next);
      } catch {
        sinon.assert.calledWith(next, errorText);
      }
    });
  });

  describe('editShoppingCentres route handler', () => {
    beforeEach(() => {
      const mockFindOneAndUpdate = {
        exec: sinon.stub().onFirstCall().resolves(shoppingCentreData).onSecondCall()
          .rejects(errorText),
      };
      sinon.stub(ShoppingCentres, 'findOneAndUpdate').returns(mockFindOneAndUpdate);
      req = {
        body: shoppingCentreData,
        params: { id: 'testId' },
      };
    });

    afterEach(() => {
      ShoppingCentres.findOneAndUpdate.restore();
    });

    it('should return the updated data ', async () => {
      await ShoppingCentresHandle.editShoppingCentres(req, res);
      sinon.assert.calledWith(res.json, shoppingCentreData);
    });

    it('should pass the error text if an error is thrown', async () => {
      try {
        await ShoppingCentresHandle.editShoppingCentres(req, res, next);
      } catch {
        sinon.assert.calledWith(next, errorText);
      }
    });
  });
});
