
const sinon = require('sinon');
const validator = require('express-validator/check');
const validationHandler = require('../validations');

describe('validation route handler', () => {
  describe('with validation errors', () => {
    let expectedError;
    let validatorFunctions;
    let req;
    let res;
    let next;
    beforeEach(() => {
      expectedError = {
        'address.street': {
          location: 'body',
          msg: 'state of shopping is required',
          param: 'address.street',
        },
        'address.city': {
          location: 'body',
          msg: 'state of shopping is required',
          param: 'address.city',
        },
        'address.state': {
          location: 'body',
          msg: 'State is not valid',
          param: 'address.state',
        },
      };
      validatorFunctions = {
        isEmpty: () => false,
        mapped: () => expectedError,
      };

      sinon.stub(validator, 'validationResult').returns(validatorFunctions);

      req = { params: { } };
      res = {
        json: sinon.spy(),
        status: sinon.stub().returnsThis(),
      };
      next = sinon.spy();
    });

    afterEach(() => {
      validator.validationResult.restore();
    });

    it('should return the correct status and the expected error data ', async () => {
      await validationHandler(req, res, next);
      sinon.assert.calledWith(res.status, 400);
      sinon.assert.calledWith(res.json, { errors: expectedError });
    });
  });

  describe('with no validation errors', () => {
    let validatorFunctions;
    let req;
    let res;
    let next;
    beforeEach(() => {
      validatorFunctions = {
        isEmpty: () => true,
      };

      sinon.stub(validator, 'validationResult').returns(validatorFunctions);

      req = {};
      res = {};
      next = sinon.spy();
    });

    afterEach(() => {
      validator.validationResult.restore();
    });

    it('should called the next function ', async () => {
      await validationHandler(req, res, next);
      sinon.assert.called(next);
    });
  });
});
