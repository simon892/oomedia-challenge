import React from 'react';
import { render } from 'react-dom';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
} from 'react-router-dom';

import App from './components/App/App';
import NotFound from './components/App/NotFound';
import ShoppingCentres from './components/ShoppingCentres';
import Home from './components/Home';
import Assets from './components/Assets';

import './styles/styles.scss';

render((
  <Router>
    <App>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/page/shoppingCentres" component={ShoppingCentres} />
        <Route path="/page/assets" component={Assets} />
        <Route component={NotFound} />
      </Switch>
    </App>
  </Router>
), document.getElementById('app'));
