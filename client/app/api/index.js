import axios from 'axios';
// import qs from 'qs';

/**
 * @method getShoppingCentres
 *
 * @returns {Promise}
 */
export function getCenters() {
  return axios.get('/api/shoppingcentres');
}


/**
 * @method addShoppingCentres
 *
 * @returns {Promise}
 */
export function addCenters(payload) {
  return axios.post('/api/shoppingcentres', payload);
}

/**
 * @method deleteShoppingCentres
 *
 * @returns {Promise}
 */
export function deleteCenters(id) {
  return axios.delete(`/api/shoppingcentres/${id}`);
}

/**
 * @method getAssets
 *
 * @returns {Promise}
 */
export function getAssets() {
  return axios.get('/api/assets');
}

/**
 * @method addAssets
 *
 * @returns {Promise}
 */
export function addAssets(payload) {
  return axios.post('/api/assets', payload);
}

/**
 * @method getAssetsByShopId
 * @param {string} id
 * @returns {Promise}
 */
export function getAssetsByShopId(id) {
  return axios.get(`/api/shoppingcentres/${id}/assets`);
}
