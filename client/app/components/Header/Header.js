import React from 'react';

import { Link } from 'react-router-dom';

const Header = () => (
  <header>
    <nav>
      <div>Navigation</div>
      <ul>
        <li><Link to="/">Current Inventory</Link></li>
        <li><Link to="/page/shoppingCentres">Add Shopping Centres</Link></li>
        <li><Link to="/page/assets">Add Assets</Link></li>
      </ul>
    </nav>
    <hr style={{ borderColor: 'black', borderWidth: '2px' }} />
  </header>
);

export default Header;
