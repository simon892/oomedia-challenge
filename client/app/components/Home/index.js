import React, { Component } from 'react';
import { getCenters, getAssetsByShopId } from '../../api';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    };
  }

  async componentDidMount() {
    await this.retrieveData();
  }

  retrieveData = async () => {
    try {
      let dataObj;
      const { data: centers } = await getCenters();
      if (centers.length > 0) {
        dataObj = await centers.reduce(async (acc, item) => {
          const accumulator = await acc;
          /* eslint no-underscore-dangle: 0 */
          const { data: assets } = await getAssetsByShopId(item._id);
          const payload = Object.assign(item, { assets });
          return accumulator.concat(payload);
        }, []);
      }
      this.setState({ data: dataObj });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { data } = this.state;

    if (!data) {
      return (
        <>
          <div>There is currently no data stored.</div>
          <div>Click shopping center link in navigation bar to add a shopping center.</div>
        </>
      );
    }
    return (
      <>
        <section>
          <h1>Shopping Centres with associated assets</h1>
          {data && data.map(item => (
            <div key={item.address.street}>
              <h3>Details</h3>
              <div>
                <strong>Name:&nbsp;</strong>
                {item.name}
              </div>
              <div>
                <strong>Address:&nbsp;</strong>
                {item.address.street}
              </div>
              <div>
                <strong>City:&nbsp;</strong>
                {item.address.city}
              </div>
              <div>
                <strong>State:&nbsp;</strong>
                {item.address.state}
              </div>
              <h3>Assets</h3>
              {item.assets.map(element => (
                <div key={`${element.name}`} style={{ paddingBottom: '20px' }}>
                  <div>
                    <strong>
                    Name:&nbsp;
                    </strong>
                    {element.name}
                  </div>
                  <div>
                    <strong>
                    Dimensions:&nbsp;
                    </strong>
                    {`${element.dimensions.width} by ${element.dimensions.length}`}
                  </div>
                  <div>
                    <strong>
                    Location:&nbsp;
                    </strong>
                    {`Level ${element.location.floorLevel}, Map Reference: ${element.location.mapRef}`}
                  </div>

                  <div>
                    <strong>
                    Status:&nbsp;
                    </strong>
                    {element.status ? 'Active' : 'Inactive'}
                  </div>
                </div>
              ))}
              <hr />
            </div>
          ))}
        </section>
      </>
    );
  }
}

export default Home;
