import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import { getCenters, addCenters } from '../../api';

class ShoppingCentres extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      errorMsgs: null,
    };
  }

  async componentDidMount() {
    await this.retrieveData();
  }

  retrieveData = async () => {
    try {
      const { data } = await getCenters();
      this.setState({ data });
    } catch (e) {
      console.log(e);
    }
  };

  onSubmit = async (values) => {
    const payload = {
      name: values.shop_name,
      address: {
        street: values.address_street,
        city: values.address_city,
        state: values.address_state,
      },
    };
    try {
      await addCenters(payload);
      await this.retrieveData();
      this.setState({ errorMsgs: null });
    } catch (e) {
      const { errors } = e.response.data;
      if (errors) {
        const errorMsgs = Object.keys(errors).map(key => errors[key].msg);
        this.setState({ errorMsgs });
      }
    }
  };


  render() {
    const { data, errorMsgs } = this.state;
    return (
      <>
        <section>
          {errorMsgs && (
          <>
            <h3>Errors from Server</h3>
            <ul>
              {errorMsgs.map(item => <li key={item}>{item}</li>)}
            </ul>
          </>
          )}
        </section>
        <section>
          <h1>Add shopping centre</h1>
          <Form
            onSubmit={this.onSubmit}
            render={({ handleSubmit, pristine, invalid }) => (
              <form onSubmit={handleSubmit}>
                <div>
                  <span>Name of Shopping center</span>
                  <Field name="shop_name" component="input" placeholder="Shop Name" />
                </div>
                <div>
                  <span>Street</span>
                  <Field name="address_street" component="input" placeholder="Street" />
                </div>
                <div>
                  <span>City</span>
                  <Field name="address_city" component="input" placeholder="City" />
                </div>
                <div>
                  <span>State</span>
                  <Field name="address_state" component="input" placeholder="State" />
                </div>
                <button type="submit" disabled={pristine || invalid}>
                  Submit
                </button>
              </form>
            )}
          />
        </section>
        <section>
          <h1>Current Shopping Centres</h1>
          {data && data.map(item => (
            <div key={item.address.street} style={{ paddingBottom: '20px' }}>
              <div>
                <strong>
                Name:&nbsp;
                </strong>
                {item.name}
              </div>
              <div>
                <strong>
                Address:&nbsp;
                </strong>
                {item.address.street}
              </div>
              <div>
                <strong>
                City:&nbsp;
                </strong>
                {item.address.city}
              </div>
              <div>
                <strong>
                City:&nbsp;
                </strong>
                {item.address.state}
              </div>
            </div>
          ))}
        </section>
      </>
    );
  }
}

export default ShoppingCentres;
