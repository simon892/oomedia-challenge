import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import { getAssets, addAssets, getCenters } from '../../api';

class Assets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shoppingCentres: null,
      assets: null,
      errorMsgs: null,
    };
  }

  async componentDidMount() {
    await this.retrieveData();
  }

  retrieveData = async () => {
    try {
      const { data: centers } = await getCenters();
      /* eslint no-underscore-dangle: 0 */
      const shoppingCentres = centers.map(item => ({ name: item.name, id: item._id }));
      const { data: assets } = await getAssets();
      this.setState({ shoppingCentres, assets });
    } catch (e) {
      console.log(e);
    }
  };

  onSubmit = async (values) => {
    const payload = {
      name: values.asset_name,
      shoppingcentreId: values.asset_shop_id,
      status: values.status,
      dimensions: {
        width: values.asset_width,
        length: values.asset_length,
      },
      location: {
        floorLevel: values.asset_floor_level,
        mapRef: values.asset_map_ref,
      },
    };
    try {
      await addAssets(payload);
      await this.retrieveData();
      this.setState({ errorMsgs: null });
    } catch (e) {
      const { errors } = e.response.data;
      if (errors) {
        const errorMsgs = Object.keys(errors).map(key => errors[key].msg);
        this.setState({ errorMsgs });
      }
    }
  };


  render() {
    const { shoppingCentres, assets, errorMsgs } = this.state;
    return (
      <>
        <section>
          {errorMsgs && (
          <>
            <h3>Errors from Server</h3>
            <ul>
              {errorMsgs.map(item => <li key={item}>{item}</li>)}
            </ul>
          </>
          )}
        </section>
        <section>
          <h1>Add assets</h1>
          <Form
            onSubmit={this.onSubmit}
            render={({ handleSubmit, pristine, invalid }) => (
              <form onSubmit={handleSubmit}>
                <div><strong>Select from avaliable Shopping centres</strong></div>
                <Field name="asset_shop_id" component="select">
                  <option />
                  { shoppingCentres && shoppingCentres.map(item => <option key={item.name} value={item.id}>{item.name}</option>)}
                </Field>
                <div>
                  <div><strong>Name of Asset</strong></div>
                  <Field name="asset_name" component="input" placeholder="Name of asset" />
                </div>
                <div><strong>Asset Dimension</strong></div>
                <div>
                  <span>Width of Asset</span>
                  <Field name="asset_width" component="input" placeholder="Width of asset" />
                </div>
                <div>
                  <span>Length of Asset</span>
                  <Field name="asset_length" component="input" placeholder="Length of asset" />
                </div>
                <div><strong>Location of Asset</strong></div>
                <div>
                  <span>Floor Level</span>
                  <Field name="asset_floor_level" component="input" placeholder="Floor level" />
                </div>
                <div>
                  <span>Map Reference</span>
                  <Field name="asset_map_ref" component="input" placeholder="Map reference" />
                </div>
                <div><strong>Status</strong></div>
                <div>
                  <span>Active?</span>
                  <Field name="status" component="input" type="checkbox" />
                </div>
                <button type="submit" disabled={pristine || invalid}>
                  Submit
                </button>
              </form>
            )}
          />
        </section>

        <section>
          <h1>Current Assets</h1>
          {assets && assets.map(element => (
            <div key={`${element.name}`} style={{ paddingBottom: '20px' }}>
              <div>
                <strong>
                    Name:&nbsp;
                </strong>
                {element.name}
              </div>
              <div>
                <strong>
                    Dimensions:&nbsp;
                </strong>
                {`${element.dimensions.width} by ${element.dimensions.length}`}
              </div>
              <div>
                <strong>
                    Location:&nbsp;
                </strong>
                {`Level ${element.location.floorLevel}, Map Reference: ${element.location.mapRef}`}
              </div>
              <div>
                <strong>
                    Shopping ID:&nbsp;
                </strong>
                {element.shoppingcentreId}
              </div>
              <div>
                <strong>
                    Status:&nbsp;
                </strong>
                {element.status ? 'Active' : 'Inactive'}
              </div>
            </div>
          ))}
        </section>

      </>
    );
  }
}

export default Assets;
